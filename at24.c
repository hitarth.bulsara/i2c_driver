
#include <linux/init.h>
#include <linux/module.h>
#include <linux/i2c.h>
#include <linux/platform_data/at24.h>

static const struct i2c_device_id at24_ids[] = {
	{ "at24c256",	0 },
	{ /* END OF LIST */ }
};
MODULE_DEVICE_TABLE(i2c, at24_ids);

struct mutex lock;

static int at24_read(struct i2c_client *client, unsigned int off, void *val, size_t count)
{
	char *buf = val;						// pointer to the buffer where value has to be stored
	struct i2c_msg msg[2];					// message structure for i2c messages
	int i;
	u8 msgbuf[2];							// stores the base address

	if (unlikely(!count))
		return count;

	mutex_lock(&lock);


	i=0;
	msgbuf[i++] = off >> 8;					// big endian to little endian
	msgbuf[i++] = off;						// address transfer

	msg[0].addr = client->addr;
	msg[0].buf = msgbuf;					// address
	msg[0].len = i;

	msg[1].addr = client->addr;
	msg[1].flags = I2C_M_RD;				// read request command
	msg[1].buf = buf;						// pointer to the buffer
	msg[1].len = count;

	i2c_transfer(client->adapter, msg, 2);	// i2c handle

	mutex_unlock(&lock);
	return 0;
}



static int at24_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	int i;
	u8 *test_string;							// test string to be read

	test_string=devm_kzalloc(&client->dev, sizeof(u8)*10, GFP_KERNEL);

	at24_read(client, 0, test_string, 10);		// reading a string
	
	pr_info("test_string\n");
	for(i=0;i<10;i++)
		pr_info("%x",*(test_string+i));			// printing the fetched string
	pr_info("\n");

	return 0;
}

static int at24_remove(struct i2c_client *client)
{

	return 0;
}

static struct i2c_driver at24_driver = {
	.driver = {
		.name = "at24",
	},
	.probe = at24_probe,								// pointer to probe function
	.remove = at24_remove,								// pointer to remove function
	.id_table = at24_ids,								// list of i2c_device ids
};


static int __init at24_init(void)
{
	return i2c_add_driver(&at24_driver);				// register the driver so that it is visible in lsmod
}
module_init(at24_init);

static void __exit at24_exit(void)
{
	i2c_del_driver(&at24_driver);						// remove the entry from lsmod
}

module_exit(at24_exit);

MODULE_LICENSE("GPL");
