# I2C Driver
Task - Write an i2c driver to read from the onboard atmel EEPROM.
[I²C] is a is a multi-master, multi-slave, packet switched, single-ended, serial computer bus invented by Philips Semiconductor (now [NXP Semiconductors]). It is typically used for attaching lower-speed peripheral ICs to processors and microcontrollers in short-distance, intra-board communication.

I²C uses only two bidirectional open-drain lines, Serial Data Line (SDA) and Serial Clock Line (SCL), pulled up with resistors. Typical voltages used are +5 V or +3.3 V.

## Driver Explained
```sh
static int __init at24_init(void)
{
	return i2c_add_driver(&at24_driver);
}
module_init(at24_init);
```
This snipet will register your i2c driver into the list of i2c drivers.
```sh
static void __exit at24_exit(void)
{
	i2c_del_driver(&at24_driver);						// remove the entry from lsmod
}
module_exit(at24_exit);
```
This snipet will remove your driver entry.
```sh
static struct i2c_driver at24_driver = {
   .driver = {
		.name = "at24",
	},
};
```
But for your init to be called you need to write this snippet which contains details about your driver, like the name of the driver. 

You can run this code and notice how after `insmod driver_name.ko` your driver will be visible under lsmod listing. On executing `rmmod driver_name.ko` the entry will be removed from lsmod.

Now you need to add your probe function which will be called when a device is plugged in. So modify `at24_driver` struct as below
```sh
static struct i2c_driver at24_driver = {
	.driver = {
		.name = "at24",
	},
	.probe = at24_probe,								// pointer to probe function
	.remove = at24_remove,								// pointer to remove function
	.id_table = at24_ids,								// list of i2c_device ids
};
```
Few details are added here like, the pointer to its probe and remove function and even the list of devices it supports.

Now to test this updates, you need to write your own probe and remove function. Add the following snippet to your code.

```sh
static const struct i2c_device_id at24_ids[] = {
	{ "at24c256",	0 },
	{ /* END OF LIST */ }
};
MODULE_DEVICE_TABLE(i2c, at24_ids);
static int at24_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
    pr_info("%s %d",__func__,__LINE__);
    return 0;
}
static int at24_remove(struct i2c_client *client)
{
    pr_info("%s %d",__func__,__LINE__);
	return 0;
}
```
Now again compile your code and run `insmod driver_name.ko` you will see a flash message like **at24_probe 50** (where at24_probe indicates the name of the function that called pr_info and 50 is the line number of that particular pr_info statement) 

On executing `rmmod driver_name.ko` you will see a similar flash message. 

If you reach this point it means that all the setup is good.
> Note : Here it is assumed that the you have modified the compatible property for eeprom to `at24c256` in .dtsi file. If it doesnot happen follow these steps.
goto ~src/linux/arch/arm/boot/dts/
open *am335x-bone-common.dtsi* file and look for *&i2c0*, you will see something like this *baseboard_eeprom: baseboard_eeprom@50* under that write the 
`compatible = "at24c256";`
This will probably solve your issue
>
Now modify your probe to the code given in this repo and add  at24_read function as well. in short copy the entire code. run it and you will have the details of first 10 bytes of your eeprom.

# UART
Task - To write a uart driver for transferring data from Beagle bone to other uart enabled device(in our case, it is PC).
Steps given in this [tutorial] is very clear and explanation is intensively given. start from page number 34.

# MEGA Exercise
Task - Write a driver for Linux kernel to blink status LED on BeagleBone Black every 3 seconds. When the driver module is inserted, LED should stay on for 3 seconds and should stay off for 3 seconds. No user space application is required, only driver should do the work. Insertion and removal of driver module should work flawless.

Here, as per our task the led should keep on blinking at every 6 seconds. Thus this task has to be done in background. There are many methods that we can choose to achieve this, the approach chosed here is timer interrupt.
```sh
int init_module(void)
{
	int err = gpio_request(53, "Power led");						
	if(err)
		printk(KERN_INFO "Error in gpio request\n");
	err = gpio_direction_output(53, 1);								 
	if(err)
		printk(KERN_INFO "Error in direction gpio pin\n");
}
```
`gpio_request()` will ask for gpio request i.e. get access over the gpio. Passing 1 as second argument to `gpio_direction_output()` will keep that pin in output mode. After executing this code you will have access over the gpio led. To turn on the led you can write `gpio_set_value(53,1);`

Now we need to setup timer. Following line will do it.
```sh
setup_timer(&my_timer, my_timer_callback, 0); // setup timer to call my_timer_callback
```
To register a callback write the following statement
```sh
mod_timer(&my_timer, jiffies + msecs_to_jiffies(3000)); // setup timer to 3000 msecs
```
my_timer_callback function can be given as follows
```sh
void my_timer_callback( unsigned long data )
{
    if(flag==0)
    {
    	gpio_set_value(53,1);						    // make led ON 
    	flag=1;                                         // store the led status
    }
    else{
    	gpio_set_value(53,0);							// make led OFF
    	flag=0;
    }
	mod_timer(&my_timer, jiffies + msecs_to_jiffies(3000));
	                                           // setup timer interval to 3000 msecs
}
```



   [tutorial]: <http://free-electrons.com/doc/training/linux-kernel/linux-kernel-labs.pdf>
   [I²C]: <https://en.wikipedia.org/wiki/I%C2%B2C>
   [NXP Semiconductors]: <http://www.nxp.com/>
